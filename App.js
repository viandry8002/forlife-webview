// import library
import React, { useEffect } from 'react';
import { PermissionsAndroid, StyleSheet } from 'react-native';
import ProgressWebView from "react-native-progress-webview";

// main function
const App = () => {

  // permission
  useEffect(() => {
    async function permission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "Cool Photo App Camera Permission",
            message:
              "Cool Photo App needs access to your camera " +
              "so you can take awesome pictures.",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the camera");
        } else {
          console.log("Camera permission denied");
        }
      } catch (error) {
        console.warn(error);
      }

      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: "IndoForlife",
            message:
              "IndoForlife access to your location",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the location");
        } else {
          console.log("Location permission denied");
        }
      } catch (error) {
        console.warn(error);
      }
    }

    permission()
  }, [])


  // main return
  return (
    <ProgressWebView source={{ uri: 'http://indoforlife.com/' }} />
  )
}

export default App;

const styles = StyleSheet.create({});
